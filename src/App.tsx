import * as React from 'react';
import {ChakraProvider, theme} from '@chakra-ui/react';
import Routes from './routes';
import {BrowserRouter} from 'react-router-dom';

export const App = () => (
  <BrowserRouter>
    <ChakraProvider theme={theme}>
      <Routes />
    </ChakraProvider>
  </BrowserRouter>
);
