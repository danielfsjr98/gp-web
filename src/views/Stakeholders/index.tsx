import {
  Stack,
  TableContainer,
  Table,
  TableCaption,
  Text,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from '@chakra-ui/react';
import React from 'react';

const stakeholdersRegister = [
  {
    id: '1',
    stakeholder: 'Daniel Ferreira',
    area: 'Tecnologia',
    position: 'Desenvolvimento de software',
    email: 'daniel@gmail.com',
    requeriments:
      'Receber feedback a cada duas semanas da área de negócio referente ao produto desenvolvido',
  },
  {
    id: '2',
    stakeholder: 'João Leonardo',
    area: 'Negócios',
    position: 'Product Owner',
    email: 'joao@gmail.com',
    requeriments: 'Receber atualizações diárias sobre a utilização do produto',
  },
  {
    id: '3',
    stakeholder: 'Ana Cláudia Martins',
    area: 'Legislativa',
    position: 'Advogada Senior',
    email: 'anaclaudia@gmail.com',
    requeriments:
      'Receber atas das reuniões de projeto para verificar se está de acordo com a legislação',
  },
  {
    id: '4',
    stakeholder: 'Bruno Leme',
    area: 'Controladoria',
    position: 'Gerente de controladoria',
    email: 'brunoleme@gmail.com',
    requeriments: 'Receber semalmente analítico financeiro da empresa',
  },
  {
    id: '5',
    stakeholder: 'Sandra Regina Leme',
    area: 'Sustentação',
    position: 'Gerente de sustentação',
    email: 'sandraleme@gmail.com',
    requeriments:
      'Receber semalmente tickets sobre problemas/sugestões na plataforma',
  },
];

const Stakeholders: React.FC = () => {
  return (
    <Stack spacing={3} textAlign='justify' pb='4'>
      <Text fontSize='2xl' fontWeight='bold'>
        Gestão das partes interessadas
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Registro das partes interessadas
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Registro das partes interessadas</TableCaption>
          <Thead>
            <Tr>
              <Th>Cód</Th>
              <Th>Parte interessada</Th>
              <Th>Área</Th>
              <Th>Função no projeto</Th>
              <Th>Canal</Th>
              <Th>Requisitos de comunicação</Th>
            </Tr>
          </Thead>
          <Tbody>
            {stakeholdersRegister.map(el => (
              <Tr key={el.id}>
                <Td>{el.id}</Td>
                <Td>{el.stakeholder}</Td>
                <Td>{el.area}</Td>
                <Td>{el.position}</Td>
                <Td>{el.email}</Td>
                <Td>{el.requeriments}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Stack>
  );
};

export default Stakeholders;
