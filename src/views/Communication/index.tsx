import {
  Stack,
  TableContainer,
  Table,
  TableCaption,
  Text,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from '@chakra-ui/react';
import React from 'react';

const stakeholdersRegister = [
  {
    id: '1',
    stakeholder: 'Daniel Ferreira',
    area: 'Tecnologia',
    position: 'Desenvolvimento de software',
    email: 'daniel@gmail.com',
    requeriments:
      'Receber feedback a cada duas semanas da área de negócio referente ao produto desenvolvido',
  },
  {
    id: '2',
    stakeholder: 'João Leonardo',
    area: 'Negócios',
    position: 'Product Owner',
    email: 'joao@gmail.com',
    requeriments: 'Receber atualizações diárias sobre a utilização do produto',
  },
  {
    id: '3',
    stakeholder: 'Ana Cláudia Martins',
    area: 'Legislativa',
    position: 'Advogada Senior',
    email: 'anaclaudia@gmail.com',
    requeriments:
      'Receber atas das reuniões de projeto para verificar se está de acordo com a legislação',
  },
  {
    id: '4',
    stakeholder: 'Bruno Leme',
    area: 'Controladoria',
    position: 'Gerente de controladoria',
    email: 'brunoleme@gmail.com',
    requeriments: 'Receber semalmente analítico financeiro da empresa',
  },
  {
    id: '5',
    stakeholder: 'Sandra Regina Leme',
    area: 'Sustentação',
    position: 'Gerente de sustentação',
    email: 'sandraleme@gmail.com',
    requeriments:
      'Receber semalmente tickets sobre problemas/sugestões na plataforma',
  },
];
const matrixCommunication = [
  {
    id: 1,
    informacao: 'Andamento do desenvolvimento de TI',
    proposito: 'Atualizar sobre status do desenvolvimento do software',
    responsavel: 'Gerente de TI',
    quem: 'Gerente de negócios',
    periodicidade: 'Semanal',
    armazenado: 'E-mail corporativo',
    procedimento: 'Esse documento deve ser salvo no drive compartilhado',
  },
  {
    id: 2,
    informacao: 'Andamento das finanças',
    proposito: 'Atualizar sobre status financeiro',
    responsavel: 'Gerente de controladoria',
    quem: 'Gerente de negócios',
    periodicidade: 'Quinzenal ou quando houver algo urgente',
    armazenado: 'E-mail corporativo',
    procedimento: 'Esse documento deve ser salvo no drive compartilhado',
  },
  {
    id: 3,
    informacao: 'Atualização dos clientes',
    proposito: 'Atualizar sobre status da área de negócio',
    responsavel: 'Gerente de negócios',
    quem: 'Gerente de TI, Gerente de controladoria, CEO',
    periodicidade: 'Quinzenal',
    armazenado: 'E-mail corporativo',
    procedimento: 'Esse documento deve ser salvo no drive compartilhado',
  },
  {
    id: 4,
    informacao: 'Andamento da parte legislativa',
    proposito: 'Atualizar sobre status da área legislativa',
    responsavel: 'Advogada Senior',
    quem: 'Gerente de controladoria, CEO',
    periodicidade: 'Semanal',
    armazenado: 'E-mail corporativo',
    procedimento: 'Esse documento deve ser salvo no drive compartilhado',
  },
  {
    id: 5,
    informacao: 'Reporte de sustentação',
    proposito: 'Atualizar sobre uso/problemas do software',
    responsavel: 'Gerente de sustentação',
    quem: 'Gerente de TI, CEO',
    periodicidade: 'Sempre que houver necessidade',
    armazenado: 'E-mail corporativo',
    procedimento: 'Esse documento deve ser salvo no drive compartilhado',
  },
];

const Communication: React.FC = () => {
  return (
    <Stack spacing={3} textAlign='justify' pb='4'>
      <Text fontSize='2xl' fontWeight='bold'>
        Gestão de comunicação
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Registro das partes interessadas
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Registro das partes interessadas</TableCaption>
          <Thead>
            <Tr>
              <Th>Cód</Th>
              <Th>Parte interessada</Th>
              <Th>Área</Th>
              <Th>Função no projeto</Th>
              <Th>Canal</Th>
              <Th>Requisitos de comunicação</Th>
            </Tr>
          </Thead>
          <Tbody>
            {stakeholdersRegister.map(el => (
              <Tr key={el.id}>
                <Td>{el.id}</Td>
                <Td>{el.stakeholder}</Td>
                <Td>{el.area}</Td>
                <Td>{el.position}</Td>
                <Td>{el.email}</Td>
                <Td>{el.requeriments}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>

      <Text fontSize='xl' fontWeight='bold' mt='2rem !important'>
        Matriz das comunicações
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Matriz das comunicações</TableCaption>
          <Thead>
            <Tr>
              <Th>Cód</Th>
              <Th>Qual informação</Th>
              <Th>Qual propósito</Th>
              <Th>Quem é o responsável</Th>
              <Th>Quem precisa da informação</Th>
              <Th>Quando e Qual periodicidade</Th>
              <Th>Onde serão armazenadas</Th>
              <Th>Procedimento</Th>
            </Tr>
          </Thead>
          <Tbody>
            {matrixCommunication.map(el => (
              <Tr key={el.id}>
                <Td>{el.id}</Td>
                <Td>{el.informacao}</Td>
                <Td>{el.proposito}</Td>
                <Td>{el.responsavel}</Td>
                <Td>{el.quem}</Td>
                <Td>{el.periodicidade}</Td>
                <Td>{el.armazenado}</Td>
                <Td>{el.procedimento}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Stack>
  );
};

export default Communication;
