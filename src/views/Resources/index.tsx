import {Stack, Text, useMediaQuery} from '@chakra-ui/react';
import React from 'react';

// import { Container } from './styles';

const Resources: React.FC = () => {
  const [isLargerThan767] = useMediaQuery('(min-width: 768px)');

  React.useEffect(() => {
    const script = document.createElement('script');

    script.src = 'https://viewer.diagrams.net/js/viewer-static.min.js';
    script.async = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return (
    <Stack spacing={3} textAlign='justify' pb='4'>
      <Text fontSize='2xl' fontWeight='bold'>
        Gestão de Recursos
      </Text>
      <Text fontSize='xl' fontWeight='bold' mt='1.6rem !important'>
        Plano de gerenciamento de recursos (Ornanograma do projeto)
      </Text>
      <Stack maxWidth={isLargerThan767 ? '75vw' : '85vw'}>
        <Stack
          className='mxgraph'
          style={{maxWidth: '100%', border: '1px solid transparent'}}
          data-mxgraph='{"highlight":"#0000ff","nav":true,"resize":true,"toolbar":"zoom layers tags lightbox","edit":"_blank","xml":"&lt;mxfile host=\"Electron\" modified=\"2022-11-15T18:34:06.665Z\" agent=\"5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) draw.io/20.2.3 Chrome/102.0.5005.167 Electron/19.0.11 Safari/537.36\" etag=\"9O0Kam5Zs4UqieV-NY18\" version=\"20.2.3\" type=\"device\"&gt;&lt;diagram id=\"9KgKaJenpPiVjwinF4W7\" name=\"Página-1\"&gt;7Ztdb+I4FIZ/TS63cux8Xk6hHXal2R21FztcjdLkANEmMWtMofPr12kS8mEoaTEYukgI2SeOE79+sM85CQYZpOuvLJjPvtEIEgOjaG2QoYGxaWFs5B8UvRQWz7ELw5TFUdmoNjzGv6A0otK6jCNYtBpyShMez9vGkGYZhLxlCxijq3azCU3aV50HU5AMj2GQyNa/44jPCitGCNUHRhBPZ9Wl7epIGlStS8NiFkR01TCRO4MMGKW8KKXrASS5epUwxXn3O45u7oxBxvuc4KQ/f03+SMn494dvYTr6czT2l7+VvTwHybIc8eDur/J++UulAqPLLIK8H2SQ29Us5vA4D8L86ErMu7DNeJqImimKkzhJBjSh7PVcMpmAE4bCvuCM/gONI5HrP6G8w/IegHFY7xycuZFMwAY0Bc5eRJPyBKcCpuTMKqurxqRVTWaN+fJKW1ByMt30XCspCqWY7xAWS8IOYwZcjB2j+zgLshBiRpUqHdngRdY2pT38RBxHjdJmR2n9UpM3pOYQZkKHaRyolToAb7IVaif04GlyHKj1S229IfUDhEu2oAtRHC3TIMtLCiUHU/DtbpPcd1wSqKIbWR3Nbd2aO/ISTSmLIBPbCfsEVNvaFfYkhb9CPhr4BOpaSLe6vqTu3b/LeA65V5h/DWEB2TNNnuNUjFHxlqhFc8fXrXl1sYbokq6QRV9yD1nUMppBW8e26LCO+Y+8fGOXtXHZLi8P141mw5eyUlwPIsm/7qgq7okuWQj7N3gesCnwfb9jeZYas2BvmYTKxiAJePzcvt1tM1Ne4TuNxUA2ELio88NzO5NbDLM8q+mndzuyOx3hTkeFDlJHr6Bshn0AO3JMcMHsVEHjPnbIlR0V7MhhzwWz4/Vkx7myo4IdObi4YHacnuz4V3ZUsCO78GrYsbDfoAe9SU9e+Q5MuLEcWGXLxMB+NCvjmsm8Wnf1WjsCibgniTt8XD0kElUkklOTKIc7B5G44adGZtwi5lB+9nJh6eTCw+3pvFQsqsvtyuLkDzBEiJrkNdW5ypOkhUlHX0cOgU28hRMH7UbisAT8lkcbLcXv40UYJBco9eZHcD5ayyl4Nduv3X/z3bFQmifcaFHPFRVfd1oVS+qx4gWHeM2I4QaZ5AC/71j7tjK/b9dqc8XxfTjKz2naeW46BxYYA2J8cfPvWxvUPiDTss9vIkZ9e4/7v9C9u+mfgfBqY+7+K9/+FU1rQs7vTFR3HbqYBU1xJKs3H9cfHlcnPGY3I0c+SI/UUfdZ45Hpqe57Z/g1HKl9z+w074f4/g53RdsyTPbFuQ8GdoI0F6/4vr1A2YWkZ6c71ux36AFev99B5GTDZxReQv4MlFcccdcxMj5Szua8k9sm6rp4yoJf88S7vX0sMtx3kfGRzGF/mva/qHQh2DkdWrq+YW8n00T4xm73ZZ2YPDntoog8XWnkyrHY/8oS0kqR9BjiwxRh5OqmSE4iHURR/f6AhZsL0Q0i788lq2RL62tu0sIjBaX9kbFJu6dunPxhYES1/k9W0bz+axu5+w8=&lt;/diagram&gt;&lt;/mxfile&gt;"}'
        />
      </Stack>

      <Text fontSize='xl' fontWeight='bold' mt='1.6rem !important'>
        Estrutura analítica dos Recursos (ERA)
      </Text>
      <Stack maxWidth={isLargerThan767 ? '75vw' : '85vw'}>
        <Stack
          className='mxgraph'
          style={{maxWidth: '100%', border: '1px solid transparent'}}
          data-mxgraph='{"highlight":"#0000ff","nav":true,"resize":true,"toolbar":"zoom layers tags lightbox","edit":"_blank","xml":"&lt;mxfile host=\"Electron\" modified=\"2022-11-15T18:47:29.908Z\" agent=\"5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) draw.io/20.2.3 Chrome/102.0.5005.167 Electron/19.0.11 Safari/537.36\" etag=\"_472qWempAYidV2bW9mu\" version=\"20.2.3\" type=\"device\"&gt;&lt;diagram id=\"9KgKaJenpPiVjwinF4W7\" name=\"Página-1\"&gt;7Vtdc5s6EP01fqwHJPH1mDpp0ztNJ9PMnSZPHQyyrQYjV5Zjp7/+CvNhkEhMHBnZdzp5CFpAWEdHu2cXMYCj+eYzCxezGxrjZACseDOAlwMAbNeD4l9meS4slu3nlikjcWHbGe7IH1xeWFhXJMbLxoWc0oSTRdMY0TTFEW/YQsbounnZhCbNpy7CKVYMd1GYqNYfJOaz3Aosy9qduMZkOisf7ZRn5mF5dWFYzsKYrmsmeDWAI0Ypz4/mmxFOMvhKYPL7Pr1wtvplDKe8yw3u/OefyT9z+PDl+000v/52/RCsPhS9PIXJqhjxLaO/MKfFb+bPJRKMrtIYZ31ZA/hxPSMc3y3CKDu7FpMvbDM+T0TLFocTkiQjmlC2vRdOJtiNImFfckYfce1M7AVjK+uw+B2Ycbx5cYB2BZtgHKZzzNmzuKS4wSlJs67NVGmb1SbJL2xhQY5p1dUOPnFQIPgGNIGC5tXvFVmEczECutQKaexgP0ZtkPpgDF1XD6R2CV+xfgEyDTFUIL6jE74WI9ILb4j9SStj3cjH44lmxp4MvEj1B3i5pGGiFV1sC/p6begGImSEushrIQlexzS8vgLvl3TJByM4uABpREJx7uLH3RkyGYEm1DBQobbboHaOBXX5sBrWCq44jS8ykSBaKU1xE8cm6Pm9OFbkgoSQ6J+uWIT3uzAesinm+8iiIl5D1HkFUIaTkJOn5s9tQ7l4wi0lYiDVhLpWc0KRJ01UPszirrrskDtypI6A1FGOg9LRdtKrYb+DB6rEeQ8P8Ibw++x46BSth+K67PhyU7vs8lk/d0oNvI870CR3PF3c8UxzR/XXeriDQFBjj/Uqe7LGLWZEDAaz0paKgd3XGw87TmbNXVfb1hGYCDoy8YW4YYaJUBcTYd9MDPQyseLPjjIPDca086dksF1nb+UJT8b7IZOc8yUpdK6UA6qAusRLnD7R5IlsM1qFgaef0EIJU7dFpoIWbrjHkqlAlSeOaN7gpXgUsGKcrZ1lxMg2S/gIGTlH2KtFcDq4q4UEPaG9Lgv3BPYXnLDdYxC3OnpU8DeK63CpanlFD+lc6NezkaFlw3doyrdpAp107MrGv6JSCx3d1tjzjXI8pvTxHAvWUnyvMk9jcQaqKmr7TgBvg/vx5FQ/JdZALvuZx1vVUxXe36/PEGLb8p1TwxgoGGfNKjvAMWU4U6/jMHoU8ewMUZddc/Va1xzomrXDLtqDN6nPQ5SuRokAuypWozUA25JDuzaNYPerEaCqETTRDtnem3iXtWTNqpNYXcUntAYmmaWk0vLLyq7MEu7LG0qhBfVMLrW+ngnQEU1/rfIdE3kBZCGmfbJ9SxowEp3lTgpJmDrGgzj6X70X617IcE2uXiV1lKf3XMICUiWgprBgqhRW+qITZ5Atv1uFh7p/uSM5jhybQWoBtkrUPpE0TCNMWKjV0/eUsFU++2QSNuT04Os7F7s9U0scdd15YzZ3sB1JlR2s8JSeetZ36GjJgzEKeR0pZDZLCPw9zr0rhURHQws1++qbRJ5CovZq04TRlH84z3qTXEg1X29Cam5m57CTaSoScGD9e3+GQNuWf3JIa97bY7ZAh4xuWw0CyfEFh8vjPUL7yG6vBOTVveI8CTUXQ/r57sGVdEnbbnHrFZZoX4OO5mqI+XfpXbWuY1SoOAEauo5jewDZ0AtgkxaH5rYOhMPgxW5Rz7USR31z85nwr+F4kH23yBaUZQhq3tbVyzpWylnm17HmTPed+2TLZLfvcmjnxW8003UBHNr+7g/oWf2upKWR1fN61/zVgPmst9xUcdqFE0/WfrKf70ogX/puUKnSH0wg0dx98J1fvvtwHl79Bw==&lt;/diagram&gt;&lt;/mxfile&gt;"}'
        />
      </Stack>
    </Stack>
  );
};

export default Resources;
