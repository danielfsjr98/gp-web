import {
  Stack,
  TableContainer,
  Table,
  TableCaption,
  Text,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from '@chakra-ui/react';
import React from 'react';

const risks = [
  {
    Código: 1,
    'Descrição do Risco': 'Desvio de escopo',
    Tipo: 'Ameaça',
    Impacto: 'Alto',
    Probabilidade: 'Média',
    'Resposta ao Risco': 'Definir parâmetros claros e apresentar em reuniões',
    'Responsável pela Ação': 'Gerente do projeto',
    'Tipo de Risco': 'Execução do projeto',
  },
  {
    Código: 2,
    'Descrição do Risco': 'Baixo desempenho',
    Tipo: 'Ameaça',
    Impacto: 'Médio',
    Probabilidade: 'Média',
    'Resposta ao Risco': 'Procurar forma de motivar os stakeholders',
    'Responsável pela Ação': 'Gerente do projeto',
    'Tipo de Risco': 'Execução do projeto',
  },
  {
    Código: 3,
    'Descrição do Risco': 'Custos elevados',
    Tipo: 'Ameaça',
    Impacto: 'Médio',
    Probabilidade: 'Alta',
    'Resposta ao Risco':
      'Reestruturar/Revisar plano de gerenciamento de custos',
    'Responsável pela Ação': 'Gerente de custos',
    'Tipo de Risco': 'Execução do projeto',
  },
  {
    Código: 4,
    'Descrição do Risco': 'Prazos muito curtos',
    Tipo: 'Ameaça',
    Impacto: 'Baixo',
    Probabilidade: 'Média',
    'Resposta ao Risco': 'Reestruturar/Revisar cronograma do projeto',
    'Responsável pela Ação': 'Gerente do projeto',
    'Tipo de Risco': 'Execução do projeto',
  },
  {
    Código: 5,
    'Descrição do Risco': 'Recursos escassos',
    Tipo: "Ameaça",
    Impacto: 'Alto',
    Probabilidade: 'Média',
    'Resposta ao Risco': 'Realocar recursos entre as áreas',
    'Responsável pela Ação': 'Gerente de recursos',
    'Tipo de Risco': 'Execução do projeto',
  },
  {
    Código: 6,
    'Descrição do Risco': 'Mudanças operacionais',
    Tipo: "Ameaça",
    Impacto: 'Alto',
    Probabilidade: 'Média',
    'Resposta ao Risco': 'Reuniões e atualização de documentos',
    'Responsável pela Ação': 'Gerente do projeto',
    'Tipo de Risco': 'Operação',
  },
  {
    Código: 7,
    'Descrição do Risco': 'Queda nos preços nos serviços em nuvem',
    Tipo: "Oportunidade",
    Impacto: 'Médio',
    Probabilidade: 'Média',
    'Resposta ao Risco': 'Reunião e Planejamento para atualizar custos',
    'Responsável pela Ação': 'Gerente do projeto',
    'Tipo de Risco': 'Execução do projeto',
  },
  {
    Código: 8,
    'Descrição do Risco': 'Queda nos preços nos equipamentos',
    Tipo: "Oportunidade",
    Impacto: 'Médio',
    Probabilidade: 'Média',
    'Resposta ao Risco': 'Reunião e Planejamento para atualizar custos',
    'Responsável pela Ação': 'Gerente do projeto',
    'Tipo de Risco': 'Execução do projeto',
  },
];

const Stakeholders: React.FC = () => {
  return (
    <Stack spacing={3} textAlign='justify' pb='4' width='100%'>
      <Text fontSize='2xl' fontWeight='bold'>
        Gestão dos riscos
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Registro dos riscos
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Registro dos riscos</TableCaption>
          <Thead>
            <Tr>
              {Object.keys(risks[0]).map(el => (
                <Th key={el}>{el}</Th>
              ))}
            </Tr>
          </Thead>
          <Tbody>
            {risks.map(el => (
              <Tr key={el.Código}>
                <Td>{el.Código}</Td>
                <Td>{el['Descrição do Risco']}</Td>
                <Td>{el.Tipo}</Td>
                <Td>{el.Impacto}</Td>
                <Td>{el.Probabilidade}</Td>
                <Td>{el['Resposta ao Risco']}</Td>
                <Td>{el['Responsável pela Ação']}</Td>
                <Td>{el['Tipo de Risco']}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Stack>
  );
};

export default Stakeholders;
