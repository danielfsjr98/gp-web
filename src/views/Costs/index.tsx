import {Stack, Text, Link} from '@chakra-ui/react';

export const Costs = () => {
  return (
    <Stack spacing={3} textAlign='justify' pb='4'>
      <Text fontSize='2xl' fontWeight='bold'>
        Gestão de custos
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Custos
      </Text>

      <Link
        fontSize='lg'
        color='blue.400'
        mt='0 !important'
        href='https://docs.google.com/spreadsheets/d/1arATBPq64PaZt3FDekgvIddCbEUSAFDx/edit?usp=sharing&ouid=111533572479677485420&rtpof=true&sd=true'
        target='_blank'
      >
        Visualizar
      </Link>
      <Text fontSize='small' mt='0 !important' color='yellow.500'>
        * Necessário e-mail USP.
      </Text>
    </Stack>
  );
};
