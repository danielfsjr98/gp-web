import {
  Stack,
  TableContainer,
  Table,
  TableCaption,
  Text,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from '@chakra-ui/react';
import React from 'react';

const metrics = [
  {
    Requisito: 'Alto número de usuários acessando o sistema ao mesmo tempo',
    Indicador: 'Número de requisições por segundo',
    Meta: '100\nrequisições por segundo',
    'Técnica de Medição':
      'Contar o número de requisições que o sistema atende por segundo',
    Frequência: '1 segundo',
    'Quem Mede': 'Engenheiros de Software',
  },
  {
    Requisito:
      'Bom percentual de entregas realizadas por cada membro da equipe',
    Indicador:
      'Percentual de entregas realizadas por cada membro membro da equipe',
    Meta: 'Percentual maior que 90\n% de cada membro da equipe',
    'Técnica de Medição':
      '(total de entregas finalizadas / total de entregas pedidas) * 100',
    Frequência: 'Mensal',
    'Quem Mede': 'Gestor do projeto',
  },
];

const tools = [
  {
    Ferramenta: 'CheckList',
    'Descrição da aplicação': 'Aplicável em todo o projeto',
    'Quando aplicar': 'Aplicável em toda atualização de entrega',
    Responsável: 'Gestor do projeto',
  },
  {
    Ferramenta: 'Auditoria',
    'Descrição da aplicação': 'Aplicável na execução de cada etapa do projeto',
    'Quando aplicar': 'Semanalmente',
    Responsável: 'Auditor',
  },
];

const Quality: React.FC = () => {
  return (
    <Stack spacing={3} textAlign='justify' pb='4'>
      <Text fontSize='2xl' fontWeight='bold'>
        Gestão de qualidade
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Métricas de qualidade
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Métricas de qualidade</TableCaption>
          <Thead>
            <Tr>
              {Object.keys(metrics[0]).map(el => (
                <Th key={el}>{el}</Th>
              ))}
            </Tr>
          </Thead>
          <Tbody>
            {metrics.map(el => (
              <Tr key={el.Requisito}>
                <Td>{el.Requisito}</Td>
                <Td>{el.Indicador}</Td>
                <Td>{el.Meta}</Td>
                <Td>{el['Técnica de Medição']}</Td>
                <Td>{el.Frequência}</Td>
                <Td>{el['Quem Mede']}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>

      <Text fontSize='xl' fontWeight='bold' mt='2rem !important'>
        Ferramentas
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Ferramentas</TableCaption>
          <Thead>
            <Tr>
              {Object.keys(tools[0]).map(el => (
                <Th key={el}>{el}</Th>
              ))}
            </Tr>
          </Thead>
          <Tbody>
            {tools.map(el => (
              <Tr key={el.Ferramenta}>
                <Td>{el.Ferramenta}</Td>
                <Td>{el['Descrição da aplicação']}</Td>
                <Td>{el['Quando aplicar']}</Td>
                <Td>{el.Responsável}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
     
    </Stack>
  );
};

export default Quality;
