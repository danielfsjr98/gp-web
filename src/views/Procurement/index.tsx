import {
  Stack,
  TableContainer,
  Table,
  TableCaption,
  Text,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
} from '@chakra-ui/react';
import React from 'react';

const decisoes = [
  {
    'Entrega/Pacote de Trabalho': 'Serviço de nuvem',
    'Fazer ou adquirir': 'Comprar',
    Porque: 'Necessário para subir aplicação para produção',
    'Quem participa': 'Gerente de projeto, Arquiteto de software',
    'Como, Tipo de Contrato': 'Assinatura na empresa especializada',
  },
  {
    'Entrega/Pacote de Trabalho': 'Notebooks',
    'Fazer ou adquirir': 'Comprar',
    Porque: 'Necessário para gerenciar e desenvolver projeto',
    'Quem participa': 'Gerente do projeto',
    'Como, Tipo de Contrato': 'Compra em lojas virtuais',
  },
  {
    'Entrega/Pacote de Trabalho': 'Mesas e Cadeiras',
    'Fazer ou adquirir': 'Comprar',
    Porque: 'Necessário para montar espaço de trabalho',
    'Quem participa': 'Gerente do projeto',
    'Como, Tipo de Contrato': 'Compra em lojas virtuais',
  },
  {
    'Entrega/Pacote de Trabalho': 'Periféricos',
    'Fazer ou adquirir': 'Comprar',
    Porque: 'Necessário para fornecer aos funcionários',
    'Quem participa': 'Gerente do projeto',
    'Como, Tipo de Contrato': 'Compra em lojas virtuais',
  },
];

const especificar = [
  {
    Aquisição: 'Serviço de nuvem',
    Quando: 'Até 20/12/2022',
    Descrição: 'Necessário para subir aplicação para produção',
  },
  {
    Aquisição: 'Notebooks',
    Quando: 'Até 20/12/2022',
    Descrição: 'Equipamento para equipe',
  },
  {
    Aquisição: 'Mesas e Cadeiras',
    Quando: 'Até 20/12/2022',
    Descrição: 'Equipamento para equipe',
  },
  {
    Aquisição: 'Periféricos',
    Quando: 'Até 20/12/2022',
    Descrição: 'Equipamento para equipe',
  },
];

const responsaveis = [
  {
    'Membro da equipe': 'Gerente do projeto',
    Responsabilidades: 'Analisar e aprovar orçamento',
  },
  {
    'Membro da equipe': 'Arquiteto de software',
    Responsabilidades: 'Escolher e indicar melhor opção para serviço de nuvem',
  },
];

const riscos = [
  {
    Risco: 'Serviço de nuvem ficar indisponível',
    'Como será tratado':
      'Contatar arquiteto de software para possíveis soluções',
  },
  {
    Risco: 'Produtos com defeitos',
    'Como será tratado': 'Acionar vendedores para troca/devolução',
  },
];

const vendedores = [
  {
    'Fornecedor potencial': 'AWS',
    Motivo: 'Opção para serviço de nuvem',
  },
  {
    'Fornecedor potencial': 'Azure',
    Motivo: 'Opção para serviço de nuvem',
  },
  {
    'Fornecedor potencial': 'Kabum',
    Motivo: 'Opção para comprar notebooks e periféricos',
  },
  {
    'Fornecedor potencial': 'Casas Bahia',
    Motivo: 'Opção para comprar notebooks, periféricos, mesas e cadeiras',
  },
  {
    'Fornecedor potencial': 'Magazine Luiza',
    Motivo: 'Opção para comprar notebooks, periféricos, mesas e cadeiras',
  },
];

const Procurement: React.FC = () => {
  return (
    <Stack spacing={3} textAlign='justify' pb='4'>
      <Text fontSize='2xl' fontWeight='bold'>
        Gestão das Aquisições
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Decisões do que fazer ou adquirir
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Decisões do que fazer ou adquirir</TableCaption>
          <Thead>
            <Tr>
              {Object.keys(decisoes[0]).map(el => (
                <Th key={el}>{el}</Th>
              ))}
            </Tr>
          </Thead>
          <Tbody>
            {decisoes.map(el => (
              <Tr key={el['Entrega/Pacote de Trabalho']}>
                <Td>{el['Entrega/Pacote de Trabalho']}</Td>
                <Td>{el['Fazer ou adquirir']}</Td>
                <Td>{el.Porque}</Td>
                <Td>{el['Quem participa']}</Td>
                <Td>{el['Como, Tipo de Contrato']}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>

      <Text fontSize='xl' fontWeight='bold' mt='2rem !important'>
        Especificar produto/serviço a ser adquirido
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>
            Especificar produto/serviço a ser adquirido
          </TableCaption>
          <Thead>
            <Tr>
              {Object.keys(especificar[0]).map(el => (
                <Th key={el}>{el}</Th>
              ))}
            </Tr>
          </Thead>
          <Tbody>
            {especificar.map(el => (
              <Tr key={el.Aquisição}>
                <Td>{el.Aquisição}</Td>
                <Td>{el.Quando}</Td>
                <Td>{el.Descrição}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>

      <Text fontSize='xl' fontWeight='bold' mt='2rem !important'>
        Responsabilidades das aquisições da Equipe do Projeto
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>
            Responsabilidades das aquisições da Equipe do Projeto
          </TableCaption>
          <Thead>
            <Tr>
              {Object.keys(responsaveis[0]).map(el => (
                <Th key={el}>{el}</Th>
              ))}
            </Tr>
          </Thead>
          <Tbody>
            {responsaveis.map(el => (
              <Tr key={el['Membro da equipe']}>
                <Td>{el['Membro da equipe']}</Td>
                <Td>{el.Responsabilidades}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>

      <Text fontSize='xl' fontWeight='bold' mt='2rem !important'>
        Decisões contratuais relacionadas a riscos
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>
            Decisões contratuais relacionadas a riscos
          </TableCaption>
          <Thead>
            <Tr>
              {Object.keys(riscos[0]).map(el => (
                <Th key={el}>{el}</Th>
              ))}
            </Tr>
          </Thead>
          <Tbody>
            {riscos.map(el => (
              <Tr key={el.Risco}>
                <Td>{el.Risco}</Td>
                <Td>{el['Como será tratado']}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>

      <Text fontSize='xl' fontWeight='bold' mt='2rem !important'>
        Vendedores Selecionados
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Vendedores Selecionados</TableCaption>
          <Thead>
            <Tr>
              {Object.keys(vendedores[0]).map(el => (
                <Th key={el}>{el}</Th>
              ))}
            </Tr>
          </Thead>
          <Tbody>
            {vendedores.map(el => (
              <Tr key={el['Fornecedor potencial']}>
                <Td>{el['Fornecedor potencial']}</Td>
                <Td>{el.Motivo}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Stack>
  );
};

export default Procurement;
