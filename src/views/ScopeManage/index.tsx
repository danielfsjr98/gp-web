import {
  Stack,
  useMediaQuery,
  Text,
  Link,
  List,
  ListIcon,
  ListItem,
} from '@chakra-ui/react';
import * as React from 'react';
import {MdCheckCircle} from 'react-icons/md';

export const ScopeManage = () => {
  const [isLargerThan767] = useMediaQuery('(min-width: 768px)');

  React.useEffect(() => {
    const script = document.createElement('script');

    script.src = 'https://viewer.diagrams.net/js/viewer-static.min.js';
    script.async = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return (
    <Stack spacing={3} textAlign='justify' pb='4'>
      <Text fontSize='2xl' fontWeight='bold'>
        Gestão de escopo
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Documentação de requisitos
      </Text>
      <Link
        fontSize='lg'
        color='blue.400'
        mt='0 !important'
        href='https://drive.google.com/file/d/1O65CWAhB_385oa98MlkB9N2bH6WsAURO/view?usp=sharing'
        target='_blank'
      >
        Visualizar
      </Link>
      <Text fontSize='small' mt='0 !important' color='yellow.500'>
        * Necessário e-mail USP.
      </Text>

      <Text fontSize='xl' fontWeight='bold' mt='1.6rem !important'>
        Declaração do Escopo do Projeto
      </Text>

      <Text fontSize='lg' fontWeight='bold'>
        Objetivo do documento
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Este documento detalha e formaliza o escopo deste projeto, em todas suas
        dimensões. Deverão ser documentados neste documento as principais
        entregas e como o trabalho será desenvolvido.
      </Text>

      <Text fontSize='lg' fontWeight='bold'>
        Justificativa do projeto
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Através da observação da rotina de controle de estoque e análise de
        crédito a revendedoras de uma empresa de roupa fitness, foi notada a
        necessidade da implantação de um sistema para otimizar e assegurar essas
        tarefas.
      </Text>

      <Text fontSize='lg' fontWeight='bold'>
        Objetivos e critérios de aceitação
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Beneficiar o controle de estoque e análise de crédito da empresa. O
        critério de aceitação será a entrega do sistema, contendo essas duas
        funcionalidades como cerne da aplicação.
      </Text>

      <Text fontSize='lg' fontWeight='bold'>
        Escopo do produto
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Duas principais funcionalidades:
      </Text>
      <List fontSize='lg' mt='0 !important'>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdCheckCircle} color='green.500' />
          Entrega do controle de estoque: Essa funcionalidade permitirá que o
          administrador do sistema tenha visibilidade do que está em estoque na
          empresa e o que está consignado com as revendedoras. Para isso, o
          sistema precisara de um dashboard para o administrador, catálogo de
          produtos, montagem de estoque para consignação, cadastro de
          revendedoras.
        </ListItem>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdCheckCircle} color='green.500' />
          Análise de crédito: Deverá ser implementada a funcionalidade de
          análise de crédito de cada revendedora cadastrada no sistema. Para
          esse propósito, devera o sistema deverá integrar as APIs do SERASA,
          para auxiliar nesta análise, e devera possuir outros parâmetros.
        </ListItem>
      </List>

      <Text fontSize='lg' fontWeight='bold'>
        Limites do escopo
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Se limitará ao case da empresa de roupa fitness, ou seja, não será um
        sistema genérico.
      </Text>

      <Text fontSize='xl' fontWeight='bold' mt='1.6rem !important'>
        Linha de Base do Escopo (EAP)
      </Text>

      <Stack maxWidth={isLargerThan767 ? '75vw' : '85vw'}>
        <Stack
          className='mxgraph'
          style={{maxWidth: '100%', border: '1px solid transparent'}}
          data-mxgraph='{"highlight":"#0000ff","nav":true,"resize":true,"toolbar":"zoom layers tags lightbox","edit":"_blank","xml":"&lt;mxfile host=\"Electron\" modified=\"2022-09-18T16:50:15.964Z\" agent=\"5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) draw.io/20.2.3 Chrome/102.0.5005.167 Electron/19.0.11 Safari/537.36\" etag=\"pFqZxylVqNpVi6M5v0Ay\" version=\"20.2.3\" type=\"device\"&gt;&lt;diagram id=\"_DB8IuzBJfbJFDAEJKnI\" name=\"Página-1\"&gt;7Z1dc6M2FIZ/jWfai2b4/rjcOJv2ojtNN9vZbW86ipFtuhh5hRw7++srYRHbCGIMtmAt7WQ2IGMBOi/Sw9ELGdnjxeZXDJbzDyiCycgyos3IvhtZlulY1oj9GNHLtiTw3G3BDMcR32hX8Bh/h7zQ4KWrOILZwYYEoYTEy8PCCUpTOCEHZQBjtD7cbIqSw70uwQwKBY8TkIiln+OIzHmpZRi7D36D8Wxe7Dq0+CdPYPJ1htEq5TscWfY0/7f9eAGKyvj22RxEaL1XZL8f2WOMENkuLTZjmLDGLdpt+737mk9fDxzDlDT5wv0/fybe538/f7/7/vwV//npEa7vfjHtbTXPIFnB4jzyoyUvRRPRA1+yRQKeWNHtnCwSumrSxfU8JvBxCSbs8zVVCC3LCMCEB9qg6zRyBMQpxPw7E5QkYJnFeWXbLeZxEv0OXtCKFLsp1m754UFM4Kb2xM3X5qQ6hWgBCX6hm/AvOD6X5EshWh6R9S7glsvL5nuxLsoA19jstepdM9MF3tKntLpzYqt/ZMK5nSMcf2etmfCGKzd1to4XCUipYEFUKrpF+RXKigha8qUETglffEKEoAVfwbwNjMpwRRgtPwE8g8Um0zhJxihBLMIpSplGlihOSd5s7i39oQ05Nm7ckUvPbEzXzd06/WGbYzJGaUYwVQqrFoKMrGHWWAFviFvURW9xt7yKuHsJO8tsCdIDAXjfVqxnuN11M79Mtm38jm6HZ08/Wa7LWtMyTD/kC4b1c95ixZfp0oz9Nord0MPe7mlbXqs62rYkBslH2uWCdNbksucdNNjpZF8WtHO8v7+1Pe9k/SEa+WmSd5zzOIpgyr4HnmBy+9owxU4iOAWrpKNmnKOacbzTJMMr27XlybWBhECcAkKvYnq6maDD1+PsIM1QkOYDRtGKIDa4s2N+RFOyps0nTTO8K+kul/OrYXMYu/0OxXDOqY5G1UmQh21cPycUoMYxwarABDOoGC6cSw0XtqkxQQIm2Ea1LPqLu9UTJpjFbp7wjhA0ODRTUU3nsqci2z5NRG8PDU1qkzEyiHeQd3Aap/FobI/e+fn/dg4R7D/KFP9BxhTqMESdMDaHYdzvbRz/nEJpVJ0MpZx61/sjMoQ/OIhwNUTIgIjj942S495XrsHSENFeRa6iEOELYv0Iv63iLCav6LAs0hEeWLCQp0/ZUqaIhoASNfJQDyWC60cJ1xwcSog5Qo0SF0CJYGAo4VQl/2SghK1Ror2KQjVRwhGTph/Q5OtqKXCEQtxQowXluMGpyqxeGTf47tC4wTnVZKK5oc1VvhX3kLihKuEngxsczQ3tVXTcNHOd3CDmSd+nM5jOAY6BwuxQpwf12EHMUV0dO4TB4Njh1EyPZodWV7k/NHaoyjXJYAdXs0N7FR3PXF0lO7higux9SjCcMXBgh0zo9QkzpbChRgrKYYN7qonvB8QG75AabKNvanBPzfNoamhzkbvHPW+S416VaZLinLzR3skOOjqeubpObhATZA+sL/kPLGiDKZVmqJOAerxwql/vCnjB650XqsxymhfOzgvH7W2S416V05PDC5bmhfY68hTlBTEZWvOsBTt+mE3QUi2IqNGFehBxqlPvB4QI1xla1sET04CaIi5AEcedbZLjXpXik+KP1FmHDjryjj/3e5UU4YnJ0Y9wxSEix4cJDY9l/PVFJXaoU4Ny7OAp8EooKzx8JdQA2EG/EkoGO3jH3W2S416V7pPymKZmhy46Ov6473Wyg5goff9tFS/ha9Jh3zOZKfjKqDplqMcRCvglBY7ofSbD035JKRwxNL+k15df0tIzGV10pKhj0hdTpQ8YTeMsi1EKkoImFEtBaM8kV4cCnskyOjhW3+jgi3lBjQ7nRwd/aKZJvy/TpHWjX/DQQUeWouggZkrHaLFcEVB2QUA2i5GuntlvdSiiThfqUYQCTkrBBNF7AsLXVkopFDE0K6Xfl5XS1gmILjpS1ErpV1kpM5g+o+Q55k9fbHMQC/72KIUIQtsoeUMoYKMsPYvRfxoiEFODGiAuABBDc1EGfbko9bMYXXQUKOqiDMRsaaWLEuBvKxp8grICKFR0RdSpRDmkCBRwV/qlt0j1764MtLtSBlMEQ3NXBn25Kx3truyiI0XdlYGYO1X1fRB1ElAPGBSwUQrA0PssRqBtlFKAYWg2yqAvG6WjkxBddKSojTIUc6XCLIZCzKD9k1wWCvgny8zQ/8RFKGYENTOcnxnCofknw778k472T3bRkXWcGU6U0RFmaFCbjMFBzIV+Uu711HXB3xyGar9Pcc8qhkbVyVCDAiZJ0yj9Kc3+JyRC7ZKUwgpDc0mGfbkkXT0h0UVHirokwwqXJJqsWGah/LTF/QeQrtiTmwpBhPZJ8oZQwCcpQkTvkxRmgTGaIi5LEUOzSlIt9oYRepqivZD4BaseR1A9CYL9hGGcqmdsqNWActRgGgp4IQVs6H+ewjS0G1IGNnB9D4ob+vJDunqqopOSFDVEmoaYJ/3jKYP4WXjZQ5Q/YIFhRhsb5CsqMYV2SxYt0cAuCdPoHcZ54/Pg7Qkgv3hhMX7BTUy+sOUb1/b4+t/5ehjw1bvN3rZ3L3srDxDH9IRy+MjLUnpyvDa3WP97/8NdXfnay/5aubbtacFoBt9GE/bA0QpP4FutxoGAFMNq3Yb28TvhqnmtogzDBJD4+fCA3xDbAxvGdzL2rFIGpiyw7Ynyb+3EJVbkH1ZkhaWKtg0hVHQ+lTawl56g0qZS6CtwZilwltkycLThjtR08cg1SHVeUeRKr5exgpaBK9XjlK/cS4fNbDDNfUVhE6+Ttl1l/1ec2WCq/IpCF7hnipxQkfTAqTXI2WWoaBs4oSLpgVNrjBPopHVfeQxzLh24Yv+SA9fgxqHBX/iWRzG21TLAvd83FKeuyJVZjpsf3hSvFzw5dMerunjwLJWD59hnC15VVRcPXoO5qisKnuUfvp289Y1fuSLpd35Wg8muKw5c67FO+NM25YouHji1btmFwHUY7ITY9THaqXXbLvRzHYY74U9D9DHenXceZejhc8u3bm3Hu3JFdrmiiwdOrayL0N5txzvhTerSxzu1si5C4DqMd0LsehjvbLVwxffO1GGWK5J+g2CrBSrl9m7dYQovbZLdYdpqIYoQuA4dphC7PjpMtUBF6Oc63CAI7z7p4QbBVgtXwvBM4125IunjXbE/RQPXerwTHwCUPeA5as3+CJHrMOCJwethxHMspePXZcQTH6M555BHVzFizyvsNsdgOf+AIsi2+B8=&lt;/diagram&gt;&lt;/mxfile&gt;"}'
        />
      </Stack>
    </Stack>
  );
};
