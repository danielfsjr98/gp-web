import {Stack, Text, List, ListItem, ListIcon} from '@chakra-ui/react';
import {MdCheckCircle, MdSettings, MdTimer} from 'react-icons/md';

export const ProjectCharter = () => {
  return (
    <Stack spacing={3} textAlign='justify' pb='4'>
      <Text fontSize='2xl' fontWeight='bold'>
        Project Charter
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Finalidade do projeto
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Facilitar o controle de estoque de loja fitness para revendedoras.
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Objetivos mensuráveis do projeto e critérios de sucesso relacionados
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Reduzir em zero o número de problemas com estoque para revendedoras,
        após a implantação, como nos casos onde estes acabam sumindo com os
        produtos e não é possível localizar com quem estava.
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Requisitos de alto nível
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Atender loja fitness que possuem revendedoras.
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Descrição de alto nível do projeto, seus limites e entregas-chave
      </Text>
      <List fontSize='lg' mt='0 !important'>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdCheckCircle} color='green.500' />
          Limites: Os limites podem estar relacionados principalmente aos
          recursos computacionais, o que leva a um maior orçamento, mas isso vai
          depender muito do número de clientes.
        </ListItem>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdCheckCircle} color='green.500' />
          Entregas-chave: Controle de estoque, gerenciamento e análise de
          crédito das revendedoras.
        </ListItem>
      </List>

      <Text fontSize='xl' fontWeight='bold'>
        Resumo do risco geral
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Em alguns casos, do ponto de vista do cliente, pode não compensar
        financeiramente, algo que no curto prazo pode ser verdade.
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Resumo do cronograma de marcos
      </Text>
      <List fontSize='lg' mt='0 !important'>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdTimer} color='green.500' />
          Até 23/09: sistema de identificação das lojas fitness.
        </ListItem>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdTimer} color='green.500' />
          Até 30/09: gerenciamento de revendedoras (criação, consulta, remoção,
          etc).
        </ListItem>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdTimer} color='green.500' />
          Até 21/10: controle de estoque, desde a chegada de um novo produto até
          seu tratamento por parte das revendedoras.
        </ListItem>

        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdTimer} color='green.500' />
          Até 04/11: Análise de crédito das revendedoras, com o auxílio da api
          do Serasa.
        </ListItem>

        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdTimer} color='green.500' />
          Até 18/11: Fazer um teste de implantação com um primeiro cliente.
        </ListItem>

        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdTimer} color='green.500' />
          Até 21/11: Atrair novos clientes, com propagandas em redes sociais,
          por exemplo.
        </ListItem>
      </List>

      <Text fontSize='xl' fontWeight='bold'>
        Recursos financeiros pré-aprovados
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Não possui recurso financeiro pré-aprovado.
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Lista das partes interessadas chave
      </Text>
      <List fontSize='lg' mt='0 !important'>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdCheckCircle} color='green.500' />
          Empresa beneficiada;
        </ListItem>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdCheckCircle} color='green.500' />
          Administradores do sistema;
        </ListItem>
        <ListItem>
          <ListIcon as={MdCheckCircle} color='green.500' />
          Desenvolvedores do sistema;
        </ListItem>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdCheckCircle} color='green.500' />
          Gerente do projeto.
        </ListItem>
      </List>

      <Text fontSize='xl' fontWeight='bold'>
        Requisitos para aprovação do projeto
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        Para finalizarmos o projeto, precisamos que o cliente utilize nosso
        sistema, de modo a resolver seu problema.
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Critérios de término do projeto
      </Text>
      <Text fontSize='lg' mt='0 !important'>
        O sistema precisa ser entregue, cumprindo todas as funcionalidades
        acordadas, para a empresa.
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Gerente do projeto designado, responsabilidade e nível de autoridade
      </Text>
      <List fontSize='lg' mt='0 !important'>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdSettings} color='green.500' />
          Daniel Ferreira;
        </ListItem>
        <ListItem display='flex' alignItems='center'>
          <ListIcon as={MdSettings} color='green.500' />
          João Leonardo.
        </ListItem>
      </List>
    </Stack>
  );
};
