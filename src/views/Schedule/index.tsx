import {
  Table,
  Stack,
  useMediaQuery,
  Text,
  TableContainer,
  TableCaption,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from '@chakra-ui/react';
import * as React from 'react';

const todos = [
  {id: '#at1', todo: 'Definição do escopo do produto'},
  {id: '#at2', todo: 'Reunião com arquitetos de software'},
  {id: ' #at3', todo: 'Contratação de equipe de engenharia de software'},
  {id: '#at4', todo: 'Contratação profissional de UX'},
  {id: '#at5', todo: 'Implementar infraestrutura'},
  {id: '#at6', todo: 'Reunião com profissional de UX'},
  {id: '#at7', todo: 'Desenvolver mockup do sistema'},
  {id: '#at8', todo: 'Planejar o desenvolvimento do produto'},
  {id: '#at9', todo: 'Realizar desenvolvimento do produto'},
  {id: '#at10', todo: 'Realizar testes do produto'},
  {id: '#at11', todo: 'Elaborar a documentação'},
  {id: '#at12', todo: 'Elaborar manual do usuário'},
  {id: '#at13', todo: 'Treinamento do usuário'},
  {id: '#at14', todo: 'Sustentação/suporte do produto'},
];

const milestones = [
  {description: 'Definição do projeto', required: 'Sim'},
  {description: 'Documentação de requisitos do produto', required: 'Sim'},
  {description: 'Mockup do produto', required: 'Sim'},
  {description: 'Engenharia do produto', required: 'Sim'},
  {description: 'Entrega e testes', required: 'Sim'},
];

const estimates = [
  {id: '#at1', description: '1 semana'},
  {id: '#at2', description: '2 - 3 dias'},
  {id: '#at3', description: '1 semana'},
  {id: '#at4', description: '1 semana'},
  {id: '#at5', description: '2 semanas'},
  {id: '#at6', description: '2-3 dias'},
  {id: '#at7', description: '2 semanas'},
  {id: '#at8', description: '1 semana'},
  {id: '#at9', description: '10 semanas'},
  {id: '#at10', description: '1 semana'},
  {id: '#at11', description: '2-3 dias'},
  {id: '#at12', description: '2-3 dias'},
  {id: '#at13', description: '2 dias'},
  {id: '#at14', description: 'A combinar'},
];

export const Schedule = () => {
  const [isLargerThan767] = useMediaQuery('(min-width: 768px)');

  React.useEffect(() => {
    const script = document.createElement('script');

    script.src = 'https://viewer.diagrams.net/js/viewer-static.min.js';
    script.async = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return (
    <Stack spacing={3} textAlign='justify' pb='4'>
      <Text fontSize='2xl' fontWeight='bold'>
        Gestão de cronograma
      </Text>

      <Text fontSize='xl' fontWeight='bold'>
        Lista de atividades
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Lista de atividades</TableCaption>
          <Thead>
            <Tr>
              <Th>Identificador</Th>
              <Th>Atividade</Th>
            </Tr>
          </Thead>
          <Tbody>
            {todos.map(el => (
              <Tr key={el.id}>
                <Td>{el.id}</Td>
                <Td>{el.todo}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>

      <Text fontSize='xl' fontWeight='bold' mt='1.6rem !important'>
        Lista de marcos
      </Text>

      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Lista de marcos</TableCaption>
          <Thead>
            <Tr>
              <Th>Descrição</Th>
              <Th>Obrigatório</Th>
            </Tr>
          </Thead>
          <Tbody>
            {milestones.map(el => (
              <Tr key={el.description}>
                <Td key={el.description}>{el.description}</Td>
                <Td>{el.required}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>

      <Text fontSize='xl' fontWeight='bold' mt='1.6rem !important'>
        Diagramas de rede
      </Text>
      <Stack maxWidth={isLargerThan767 ? '75vw' : '85vw'}>
        <Stack
          className='mxgraph'
          style={{maxWidth: '100%', border: '1px solid transparent'}}
          data-mxgraph='{"highlight":"#0000ff","nav":true,"resize":true,"toolbar":"zoom layers tags lightbox","edit":"_blank","xml":"&lt;mxfile host=\"Electron\" modified=\"2022-09-23T22:54:29.710Z\" agent=\"5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) draw.io/20.2.3 Chrome/102.0.5005.167 Electron/19.0.11 Safari/537.36\" etag=\"Un2JwMAB93VtRGyaAjhY\" version=\"20.2.3\" type=\"device\"&gt;&lt;diagram id=\"RNqgR5ij2rCXuxwNL_xw\" name=\"Página-1\"&gt;7VxNc6M4EP01Pk4KEF8+ZuJMMoetSk2ytdm9KSDbmgFEhIid/PoVWLJBciqMbUBOTQ42aoQQrafXrYfiCbhK1zcU5su/SIySiWPF6wmYTRzHtkDIvyrL68biu9ONYUFxLCrtDPf4DckrhbXEMSpaFRkhCcN52xiRLEMRa9kgpWTVrjYnSfuuOVwgzXAfwUS3/oNjttxYHQCmuxO3CC+W4tYAWKLnKZS1haFYwpisGiZwPQFXlBC2OUrXVyipvCcds7nu2ztntz2jKGNdLnh+AQ/oF4n+C95uH76zm4fbp9svopUXmJTiiSdXYBLOMhxhIvrNXqU3KCmzGFXtWRPwdbXEDN3nMKrOrvj4c9uSpQkv2fxQ75+8GaIMrRsm0d8bRFLE6CuvIs4GwnUCPI4vyqvdUNjSvcvGKMh6UIz+Ytvyzj/8QLjoN9zlaO6aoTnOcOW0y6D+BKSCe/WBiojk4zvRcU3zItBBpzoJZfFlNX15KUpgUeCo7Ze2E9Eas0dxpjr+t7JfeKI0WzeqzV5F4V3PFqSkEfp4zjBIF4h9DBYUtyhGH6fGOHh7hkHaKEogwy9tYto3NuIOdwTzJ9vCwHXaMNgSlWxi89ziqiaPqA2ps9JTGto4Rmuohsr2sQ9Hj7cHPX7CnfV1Tup+7mDkP5dEnvhS1MHlklew7XxdI0Ce50cL8V03VOQwk7YfqBTzu57ZEUnr0PJc8nnL6rmOatDM2YpjSbbAn6zZiDQ/UdXCK266Lc0js4Vrm8YWgQlswR1KXx+bhcZVVXF3WV06nmWcjizjGcUyjnUiltl26L2GemaZsD+WueL1KGRQTRoQ55QcSUbhaS/KXkjyglOOnXeJxnxG8YzL4qbDRpD3xjunZI45WZEMVl3++/Hsg4c61CAce6hlNDtd9KgcLtaptn1EZBg2Z/U6RpPQqGjyaXJWe88622QYBmPjcGoUDj1fgU/oHYZDX82unYFxqAsYvYa+72meoCp/gbTqWjanEBWMlqyk8NNFOxuMHu1Orqz0SzPO2DQj54MpPKNIdcA+kGe8sN2QOzTPuBoQax0F7iSUKt09ZgKryDzBhA6AcemrLnbN5LoQVZSakuhXmU+E4lzggqEUGufYcGqcY/3emfIsVKZpV6J0jSLK8FQyU+iNvDA4ubp5njiU+PoYiGbpnYGlBdoLq/GnNNgVloH7W832DdIexdC7BGboZ7082GicmuoppLK4rIrnsjZQU4nxRU97n+o5OtMczhhhV8IwS9JSI5czPVXkcoeNXBK8fZDCDwQT/PbpSMG21DzYGpsVnP6FyWH3Zhg22/1e0gMdSAOv7yVKPg1snPOMJmr6efA6SMtXwoEBpSuXD6hgqNDI3jBpw1Y3KRrA6br4ZsDkHGEHjdVxUkvwmTKp1a0vwaEposoOQ0eJfVv1jAVi0KO60fV9hBygP0g8MRJ1ufc6gU+E1muMmERl/ZJS2aFjWqhx1Z3c7uihRpcvG35NYVbWW5vqCF4WZe1Zmx77nwVD+BaMLtg4+1S3Adjzg70fI8Rx0JU9A6PY0/bsE9GnrW5AGZw/BxMPrYvpNGjhEXigF0BWjdwhirlvEB0QpL5ZIHXV0Oy5fAgafwdC1lVyB36fEV9hyOWtWanoEXD7tFTnq5swVIG8b6ToiugDRTiDLe3Z7GRG8+HoyQxwNK/elznhT8Sbuy8Ldgbpt/bqZvRdLKD//X7nofT4HYOvhKExtKnmdYfTpkrAA2eIoH/R8VDdZtjILOPHHyyOh0Vdd/yG08nuTcJPZOKbhGloXHzpfZfkEfPM6TrPzNoxZmubYQ+eZ1NVszrZPOPF3Q+TbKrvft8FXP8P&lt;/diagram&gt;&lt;/mxfile&gt;"}'
        />
      </Stack>

      <Text fontSize='xl' fontWeight='bold' mt='3rem !important'>
        Estimativas da duração
      </Text>
      <TableContainer overflowX='scroll' maxW='83vw'>
        <Table variant='striped' colorScheme='blue'>
          <TableCaption>Estimativas da duração</TableCaption>
          <Thead>
            <Tr>
              <Th>Identificador</Th>
              <Th>Duração</Th>
            </Tr>
          </Thead>
          <Tbody>
            {estimates.map(el => (
              <Tr key={el.id}>
                <Td key={el.id}>{el.id}</Td>
                <Td>{el.description}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Stack>
  );
};
