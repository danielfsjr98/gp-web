import {FiHome, FiTrendingUp} from 'react-icons/fi';
import {ProjectCharter} from '../views/ProjectCharter';
import {ScopeManage} from '../views/ScopeManage';
import {Schedule} from '../views/Schedule';
import {MdSchedule, MdAttachMoney, MdGroup} from 'react-icons/md';
import {Costs} from '../views/Costs';
import Quality from '../views/Quality';
import Resources from '../views/Resources';
import Communication from '../views/Communication';
import Risk from '../views/Risk';
import {
  FaCheckCircle,
  FaExclamationCircle,
  FaUserFriends,
  FaUsersCog,
  FaShoppingCart,
} from 'react-icons/fa';
import Procurement from '../views/Procurement';
import Stakeholders from '../views/Stakeholders';

const routes = [
  {
    path: '/',
    name: 'ProjetCharter',
    component: ProjectCharter,
    label: 'Project Charter',
    icon: FiHome,
  },
  {
    path: '/gestao-escopo',
    name: 'ScopeManage',
    component: ScopeManage,
    label: 'Gestão de escopo',
    icon: FiTrendingUp,
  },
  {
    path: '/cronograma',
    name: 'Schedule',
    component: Schedule,
    label: 'Cronograma',
    icon: MdSchedule,
  },
  {
    path: '/custos',
    name: 'Costs',
    component: Costs,
    label: 'Custos',
    icon: MdAttachMoney,
  },
  {
    path: '/qualidade ',
    name: 'Quality',
    component: Quality,
    label: 'Qualidade',
    icon: FaCheckCircle,
  },
  {
    path: '/recursos ',
    name: 'Resources',
    component: Resources,
    label: 'Recursos',
    icon: FaUsersCog,
  },
  {
    path: '/comunicacao',
    name: 'Communication',
    component: Communication,
    label: 'Comunicação',
    icon: FaUserFriends,
  },
  {
    path: '/riscos',
    name: 'Risk',
    component: Risk,
    label: 'Risco',
    icon: FaExclamationCircle,
  },
  {
    path: '/aquisicao',
    name: 'Procurement',
    component: Procurement,
    label: 'Aquisição',
    icon: FaShoppingCart,
  },
  {
    path: '/partes-interessadas',
    name: 'Stakeholders',
    component: Stakeholders,
    label: 'Partes interessadas',
    icon: MdGroup,
  },
];

export default routes;
