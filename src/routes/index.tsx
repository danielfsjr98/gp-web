import {Switch, Route, Redirect} from 'react-router-dom';
import {Box, Grid, useMediaQuery} from '@chakra-ui/react';
import {ColorModeSwitcher} from '../components/ColorModeSwitcher';
import publicRoutes from './public.routes';
import SimpleSidebar from '../components/Sidebar';

const Routes = () => {
  const [isLargerThan767] = useMediaQuery('(min-width: 768px)');

  return (
    <SimpleSidebar>
      <Box textAlign='center' fontSize='xl'>
        <Grid
          minH='100vh'
          p={3}
          pb={0}
          gridTemplateRows={isLargerThan767 ? '5px 1fr' : '1fr'}
        >
          {isLargerThan767 && <ColorModeSwitcher justifySelf='flex-end' />}
          <Switch>
            {publicRoutes.map(route => (
              <Route
                key={route.name}
                path={route.path}
                component={route.component}
                exact
              />
            ))}
            <Redirect to={'/'} />
          </Switch>
        </Grid>
      </Box>
    </SimpleSidebar>
  );
};

export default Routes;
